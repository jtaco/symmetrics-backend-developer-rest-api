## SYMMETRICS | Backend Developer | REST API

This API is hosted in a Firebase project with Firebase Functions enabled.

This is the url: **https://us-central1-symmetrics-rest-api.cloudfunctions.net/app**

These are the endpoints you can use in this API:

1. Add new client (Method: POST, PATH: /clients, BODY: { "fullName": string }).
2. Add new car to client (Method: POST, PATH: /cars, BODY: { "model": string, "clientId": string}).
3. Add new car-fix to car (Method: POST, PATH: /carfix, BODY: { "description": string, "carId": string}).
4. Get all clients [asumming there is one garage only] (Method: GET, PATH: /clients).
5. Get all cars of client (Method: GET, PATH: /cars/client/:clientId).
6. Get all car-fixes of a car (Method: GET, PATH: /carfix/car/:carId).
7. Get all car-fixes in the garage in descending order (Method: GET, PATH: /carfix/bydate).