import { Response } from 'express';
import { db, addDoc, getDocs, getDoc, updateDoc, deleteDoc } from '../config/firebase';

type Type = {
    id: string,
    fullName: string
}

type Request = {
    body: Type,
    params: { id: string }
}

const COLLECTION_NAME = 'client';

export const callback = {
    get: async (id: string) => {
        const query: any = db.collection(`${COLLECTION_NAME}s`).doc(id);
        return await getDoc(query)
            .catch(error => {
                throw new Error(`There is no client with clienId: ${id}`);
            });
    }
};

export const api = {
    add: async (req: Request, res: Response) => {
        const { fullName } = req.body;
        try {
            const doc = await addDoc({ fullName }, `${COLLECTION_NAME}s`)
                .catch(error => {
                    return res.status(400).json({
                        status: 'error',
                        message: error.message
                    });
                });

            return res.status(200).send({
                status: 'success',
                message: `${COLLECTION_NAME} added successfully`,
                data: doc
            });
        } catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            }); 
        }
    },
    get: async (req: Request, res: Response) => {
        const { params: { id } } = req;
        try {
            const doc: Type = await callback.get(id)
                .catch(error => {
                    return res.status(400).json({
                        status: 'error',
                        message: error.message
                    });
                });
            return res.status(200).json(doc);
        } catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            }); 
        }
    },
    getAll: async (req: Request, res: Response) => {
        try {
            const query: any = db.collection(`${COLLECTION_NAME}s`);
            const allDocs: Type[] = await getDocs(query);
            return res.status(200).json(allDocs);
        } catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            }); 
        }
    },
    update: async (req: Request, res: Response) => {
        const { body: { fullName }, params: { id } } = req;

        try {
            const query = db.collection(`${COLLECTION_NAME}s`).doc(id);
            const currentDoc = (await getDoc(query)) || {};

            const newDoc = {
                fullName: fullName || currentDoc.fullName
            };

            await updateDoc(id, newDoc, `${COLLECTION_NAME}s`)
                .catch(error => {
                    return res.status(400).json({
                        status: 'error',
                        message: error.message
                    });
                });

            return res.status(200).json({
                status: 'success',
                message: `${COLLECTION_NAME} with id: ${id} updated successfully`,
                data: newDoc
            });
        }
        catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            });
        }
    },
    delete: async (req: Request, res: Response) => {
        const { id } = req.params;
        try {
            await deleteDoc(id, `${COLLECTION_NAME}s`).catch(error => {
                return res.status(400).json({
                    status: 'error',
                    message: error.message
                });
            });

            return res.status(200).json({
                status: 'success',
                message: `${COLLECTION_NAME} with id:${id} deleted successfully`,
            });
        }
        catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            });
        }
    }
}