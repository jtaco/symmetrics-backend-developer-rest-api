import { Response } from 'express';
import { db, getDocs, addDoc, getDoc } from '../config/firebase';
import { callback as clientCB } from './client';

type Type = {
    id: string,
    model: string,
    clientId: string,
}

type Request = {
    body: Type,
    params: { 
        id: string,
        clientId: string
    }
}

const COLLECTION_NAME = 'car';

export const callback = {
    get: async (id: string) => {
        const query: any = db.collection(`${COLLECTION_NAME}s`).doc(id);
        return await getDoc(query)
            .catch(error => {
                throw new Error(`There is no car with carId: ${id}`);
            });
    }
};

export const api = {
    get: async (req: Request, res: Response) => {
        const { params: { id } } = req;
        try {
            const doc: Type = await callback.get(id)
                .catch(error => {
                    return res.status(400).json({
                        status: 'error',
                        message: error.message
                    });
                });
            return res.status(200).json(doc);
        } catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            }); 
        }
    },
    add: async (req: Request, res: Response) => {
        const { model, clientId } = req.body;
        try {
            await clientCB.get(clientId);
                
            const doc = await addDoc({ model, clientId }, `${COLLECTION_NAME}s`);

            return res.status(200).send({
                status: 'success',
                message: `${COLLECTION_NAME} added successfully`,
                data: doc
            });
        } catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            });
        }
    },
    getAllByClientId: async (req: Request, res: Response) => {
        const { clientId } = req.params;
        try {
            await clientCB.get(clientId)
                .catch(error => {
                    return res.status(400).json({
                        status: 'error',
                        message: error.message
                    });
                });

            const query: any = db.collection(`${COLLECTION_NAME}s`)
                .where('clientId', '==', clientId);
            const allDocs: Type[] = await getDocs(query);
            return res.status(200).json(allDocs);
        } catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            }); 
        }
    }
}