import { Response } from 'express';
import { db, addDoc, getDocs, getCurrentTimestamp } from '../config/firebase';
import { callback as carCB } from './car';

type Type = {
    id: string,
    description: string,
    carId: string,
    createdAt: any
}

type Request = {
    body: Type,
    params: { 
        id: string,
        carId: string
    }
}

const COLLECTION_NAME = 'carfix';

export const api = {
    add: async (req: Request, res: Response) => {
        const { description, carId } = req.body;
        try {
            await carCB.get(carId);

            const doc = await addDoc({ description, carId, createdAt: getCurrentTimestamp() }, `${COLLECTION_NAME}s`);
            return res.status(200).send({
                status: 'success',
                message: `${COLLECTION_NAME} added successfully`,
                data: doc
            });
        } catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            });
        }
    },
    getAllByDate: async (req: Request, res: Response) => {
        try {
            const query: any = db.collection(`${COLLECTION_NAME}s`)
                .orderBy('createdAt', 'desc');
            const allDocs: Type[] = await getDocs(query);
            return res.status(200).json(allDocs);
        } catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            }); 
        }
    },
    getAllByCarId: async (req: Request, res: Response) => {
        const { carId } = req.params;
        try {
            await carCB.get(carId)
                .catch(error => {
                    return res.status(400).json({
                        status: 'error',
                        message: error.message
                    });
                });

            const query: any = db.collection(`${COLLECTION_NAME}s`)
                .where('carId', '==', carId);
            const allDocs: Type[] = await getDocs(query);
            return res.status(200).json(allDocs);
        } catch (error) { 
            return res.status(500).json({
                status: 'error',
                message: error.message
            }); 
        }
    }
}