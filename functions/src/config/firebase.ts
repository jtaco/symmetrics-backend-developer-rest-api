import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

admin.initializeApp({
  credential: admin.credential.cert({
    privateKey: functions.config().private.key.replace(/\\n/g, '\n'),
    projectId: functions.config().project.id,
    clientEmail: functions.config().client.email
  })
});

const db = admin.firestore();

const addDoc = async (doc: any, collectionName: string) => {
    const ref = db.collection('entries').doc();
    doc.id = ref.id;
    await updateDoc(ref.id, doc, collectionName);
    return doc;
}

const formatDoc = (doc:any) => {
    if (!doc.data()) throw new Error('There is no results in query');
    const data = doc.data();
    data.id = doc.id;
    return data;
}

const formatDocs = (snap:any) => {
    return snap.docs.map((doc:any) => {
        return formatDoc(doc);
    });
}

const getDocs = async (query:any) => {
    return formatDocs(await query.get());
}

const getDoc = async (query:any) => {
    return formatDoc(await query.get());
}

const updateDoc = (id: string, doc: any, collectionName: string) => {
    return db.collection(collectionName).doc(id).set(doc);
}

const deleteDoc = (id: string, collectionName: string) => {
    const doc = db.collection(collectionName).doc(id);
    return doc.delete();
}

const getCurrentTimestamp = () => {
    return admin.firestore.Timestamp.fromDate(new Date());
}

export { admin, db, addDoc, getDocs, getDoc, updateDoc , deleteDoc, getCurrentTimestamp }