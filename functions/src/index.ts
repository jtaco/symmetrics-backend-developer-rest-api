import * as functions from 'firebase-functions';
import * as express from 'express';
import { api as clientAPI } from './api/client';
import { api as carAPI } from './api/car';
import { api as carFixAPI } from './api/carFix';

const app = express();

app.get('/clients/:id', clientAPI.get);
app.patch('/clients/:id', clientAPI.update);
app.delete('/clients/:id', clientAPI.delete);


app.get('/clients', clientAPI.getAll);
app.post('/clients', clientAPI.add);

app.get('/cars/:id', clientAPI.get);
app.post('/cars', carAPI.add);
app.get('/cars/client/:clientId', carAPI.getAllByClientId);

app.get('/carfix/bydate', carFixAPI.getAllByDate);
app.post('/carfix', carFixAPI.add);
app.get('/carfix/car/:carId', carFixAPI.getAllByCarId);

exports.app = functions.https.onRequest(app);
